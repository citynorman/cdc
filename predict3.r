#setwd("C:/tempnn/cdc")
#setwd("~/Documents/jpm_cdc")

#helpers
library(foreign)
library(lattice)
library(dplyr)
library(modeest)
library(cvTools)

#models
library(tree)
library(randomForest)
library(adabag)
library(e1071)

#multi-core
library(doMC)
registerDoMC(cores = 4)

#data.all=read.xport("LLCP2014.XPT")
#data.test = data.all[sample(nrow(data.all),10000),]
#write.csv(data.sample,'sample.csv')
#data=read.csv('sample.csv')
#data=data.all
#data=read.xport("LLCP2014.XPT")
data=select(data,-X_ALTETH2,-RMVTETH3) #remove other teeth extraction columns

#output variables
out.misclass.cv=rep(0,6)
names(out.misclass.cv)=c('RF','SVM','NN','AdaB','XGB','Ensemble')

#**************************************
# feature selection + extraction
#**************************************
#explore vars
histogram(data$X_EXTETH2) # no class imbalance, no multi-class

table(data$X_EXTETH2,data$LASTPAP2)
table(data$X_EXTETH2,data$X_DENVST2)
table(data$X_EXTETH2,data$MEDICARE)
confusionMatrix(data$X_EXTETH2,data$X_SMOKER3)
confusionMatrix(data$X_EXTETH2,data$LASTDEN3)
confusionMatrix(data$X_EXTETH2,data$X_DENVST2)
#X_DENVST2,

#correlation test
kruskal.test(data$HLTHCVR1 ~ data$X_HCVU651) 

#**************************************
# feature selection
#**************************************

runKruskal=function(df,varName) {

  test.kruskal.var = rep('',ncol(df))
  test.kruskal.chi2 = rep(0,ncol(df))
  test.kruskal.pval = rep(1,ncol(df))
  
  for (iVar in 1:ncol(df) ) {
    tryCatch({
      t=kruskal.test(select(df,varName) ~ df[,iVar]) 
      test.kruskal.var[iVar] = colnames(df)[iVar]
      test.kruskal.chi2[iVar] = t$statistic
      test.kruskal.pval[iVar] = t$p.value
    }, error = function(e) {
      test.kruskal.var[iVar] = colnames(df)[iVar]
      test.kruskal.chi2[iVar] = -1
      test.kruskal.pval[iVar] = 9
    })
  }
  
  tmp.kruskal = data.frame(var=test.kruskal.var,chi2=test.kruskal.chi2, pval=test.kruskal.pval)
  tmp.kruskal = arrange(tmp.kruskal,desc(pval))
  
  return(tmp.kruskal)
}
data.kruskal=runKruskal(data,"X_EXTETH2")
  

#**************************************
# preprocess
#**************************************
#method 1: pick features with high kruskal values
collist=c(as.character(data.kruskal$var[260:278]))
collist
t=data[,collist]
data.fit= data[,collist] %>%
  mutate(A_EXTETH2=as.factor(X_EXTETH2==1)) %>%
  select(-X_EXTETH2) %>%
#  select(A_EXTETH2,X_AGE_G,EMPLOY1,MEDICARE,INCOME2,GENHLTH,EDUCA,X_HCVU651,MARITAL,X_CHLDCNT,X_SMOKER3) %>%
  na.omit()
#  mutate(MEDICARE=as.numeric(MEDICARE==1)) %>%
#  mutate(GENHLTH=as.numeric(GENHLTH==1))

#method 2: pick select features
selectFeatures2=function(df) {
  df = df %>%
    mutate(A_EXTETH2=as.factor(X_EXTETH2==1)) %>%
    select(-X_EXTETH2) %>%
    select(A_EXTETH2,X_AGE_G,EMPLOY1,MEDICARE,INCOME2,GENHLTH,EDUCA,X_HCVU651,MARITAL,X_CHLDCNT,X_SMOKER3) %>%
    na.omit() %>%
    mutate(MEDICARE=as.numeric(MEDICARE==1)) %>%
    mutate(GENHLTH=as.numeric(GENHLTH==1))
  return(df)
}
data.fit= selectFeatures2(data)
#data.fit= selectFeatures2(data.all)
data.fit.test= selectFeatures2(data.test)

barchart(data.fit$A_EXTETH2,col='dodgerblue4') # no class imbalance, no multi-class
densityplot(data.fit$X_AGE_G,col='dodgerblue4') # no class imbalance, no multi-class
summary(data.fit$X_AGE_G)

#**************************************
# trees/random forest
#**************************************
rf.fit=randomForest(A_EXTETH2~. , data=data.fit, importance=T)
summary(rf.fit)
y.fit.rf=predict(rf.fit)
table(y.fit.rf,data.fit$A_EXTETH2)
confusionMatrix(y.fit.rf,data.fit$A_EXTETH2)
importance(rf.fit)
varImpPlot(rf.fit)

rf.cv=rfcv(as.matrix(select(data.fit,-A_EXTETH2)),data.fit$A_EXTETH2)
rf.cv$error.cv
out.misclass.cv[1]=1-rf.cv$error.cv[1]

#tree
tree.fit=tree(A_EXTETH2~. , data=data.fit)
summary(tree.fit)
tree.fit
plot(tree.fit)
text(tree.fit)
cv.tree(tree.fit ,FUN=prune.misclass, K=10)

tree.fit.pruned <- prune.tree(tree.fit,best=2)
tree.fit.pruned
plot(tree.fit.pruned)
text(tree.fit.pruned)



#**************************************
# SVM
#**************************************
svm.fit = svm(A_EXTETH2~., data=data.fit, kernel='radial')
y.fit.svm=predict(svm.fit)
table(y.fit.svm,data.fit$A_EXTETH2)
confusionMatrix(y.fit.svm,data.fit$A_EXTETH2)

set.seed(1)
svm.tune=e1071::tune(svm ,A_EXTETH2~., data=data.fit ,kernel ="radial", ranges =list(cost=c(1)))
out.misclass.cv[2]=1-svm.tune$best.performance

set.seed(1)
#svm.tune=e1071::tune(svm ,A_EXTETH2~., data=data.fit ,kernel ="radial", ranges =list(cost=c(0.001 , 0.01, 0.1, 1,5,10,100), gamma=c(0.5,1,2,3,4)))
#svm.tune=e1071::tune(svm ,A_EXTETH2~., data=data.fit ,kernel ="radial", ranges =list(cost=c(0.1, 1,10), gamma=c(0.5,1,4)))
#svm.tune
# svm.tune=tune.out
# summary(svm.tune)
# svm.tune.best=svm.tune$best.model
# y.fit.svm.best=predict(svm.tune.best)
# table(y.fit.svm.best,data.fit$A_EXTETH2)
# confusionMatrix(y.fit.svm.best,data.fit$A_EXTETH2)

#neural net
nn.fit <- train(A_EXTETH2~., data=data.fit,
                method = "nnet",
                tuneGrid = expand.grid(
                  .size = c(5),
                  .decay = 0.1),
                trControl = trainControl(method = "cv"),
                maxit = 100)

out.misclass.cv[3]=mean(nn.fit$resample$Accuracy)

y.fit.nn = predict(nn.fit)
table(y.fit.nn,data.fit$A_EXTETH2)
confusionMatrix(y.fit.nn,data.fit$A_EXTETH2)

#**************************************
# adaboost
#**************************************
library(adabag)
ada.fit = boosting(A_EXTETH2~., data=data.fit)
y.fit.ada = predict(ada.fit,newdata = select(data.fit,-A_EXTETH2))$class
table(y.fit.ada,data.fit$A_EXTETH2)
confusionMatrix(y.fit.ada,data.fit$A_EXTETH2)

ada.cv=boosting.cv(A_EXTETH2~., data=data.fit)
out.misclass.cv[4]=1-ada.cv$error



#**************************************
# XG boost
#**************************************
library(xgboost)
xgb.fit =  xgboost(data = as.matrix(select(data.fit,-A_EXTETH2)), label = as.matrix(as.numeric(data.fit$A_EXTETH2)-1), max.depth = 5, eta = 1, nthread = 2, nround = 10, nfold = 10, objective = "binary:logistic")
xgb.fit.cv = xgb.cv(data = as.matrix(select(data.fit,-A_EXTETH2)), label = as.matrix(as.numeric(data.fit$A_EXTETH2)-1), max.depth = 5, eta = 1, nthread = 2, nround = 10, nfold = 10, objective = "binary:logistic")
out.misclass.cv[5]=1-mean(xgb.fit.cv$test.error.mean)

y.fit.xgb = predict(xgb.fit,newdata = as.matrix(select(data.fit,-A_EXTETH2)))>0.5
table(y.fit.xgb,data.fit$A_EXTETH2)
confusionMatrix(y.fit.xgb,data.fit$A_EXTETH2)

table(y.fit.xgb,data.fit.test$A_EXTETH2)
confusionMatrix(y.fit.xgb,data.fit.test$A_EXTETH2)

xgb.imp = xgb.importance(as.matrix(select(data.fit,-A_EXTETH2)), model = xgb.fit)
xgb.plot.importance(xgb.imp)

#**************************************
# ensemble XGB
#**************************************
data.ensemble = data.frame(actual=data.fit$A_EXTETH2,svm=y.fit.svm,nn=y.fit.nn,rf=y.fit.rf,ada=as.factor(y.fit.ada),xgb=y.fit.xgb)
data.ensemble.num = data.frame(lapply(data.ensemble, function(x) as.numeric(x)))

xgb.fit.ensemble =  xgboost(data = as.matrix(select(data.ensemble.num,-actual)), label = data.ensemble.num$actual-1, max.depth = 5, eta = 1, nthread = 2, nround = 10, nfold = 10, objective = "binary:logistic")
xgb.fit.cv.ensemble =  xgb.cv(data = as.matrix(select(data.ensemble.num,-actual)), label = data.ensemble.num$actual-1, max.depth = 5, eta = 1, nthread = 2, nround = 10, nfold = 10, objective = "binary:logistic")
out.misclass.cv[6]=1-mean(xgb.fit.cv.ensemble$test.error.mean)

#**************************************
# plotting
#**************************************
barplot(out.misclass.cv)

plot(tree.fit)
text(tree.fit)

plot(data.fit$X_AGE_G)
